#!/bin/bash

######################################################################################
### Script de modification des paramètres des fichiers de configuration de Dovecot ###
######################################################################################

### Notes
# La configuration de Dovecot étant éclatée dans plus d'une dizaine de fichiers, ce script
# est censé être une alternative à la copie de tous ces fichiers pré-édité dans le conteneur.
# Fichier par fichier, il vient éditer des paramètres spécifiques de Dovecot.
# La commande massivement utilisée dans ce script est sed. L'utilisation qui en
# est faite est principalement celle-ci :
# sed -i (édite le fichier) 's/regex identifiant la ligne à remplacer/ligne de substitution/' /chemin/du/fichier

### Modification de /etc/dovecot/dovecot-ldap.conf.ext
# Définition de l'adresse du LDAP
# L'URI peut contenir un /, on utilise @ pour séparer les champs de sed
sed -i 's@^#uris =.*@uris = '"${LDAP_ADDRESS}"'@' /etc/dovecot/dovecot-ldap.conf.ext
# Définition du distinguished name pour accéder au LDAP
sed -i 's/^#dn =.*/dn = '"${LDAP_NSS_CN}"'/' /etc/dovecot/dovecot-ldap.conf.ext
# Définition du mot de passe pour accéder au LDAP
sed -i 's/^#dnpass =.*/dnpass = '"${LDAP_DNPASS}"'/' /etc/dovecot/dovecot-ldap.conf.ext
# Autoriser les binds LDAP
sed -i 's/^#auth_bind = no/auth_bind = yes/' /etc/dovecot/dovecot-ldap.conf.ext
# Définition de la version du LDAP
sed -i 's/^#ldap_version = .*/ldap_version = 3/' /etc/dovecot/dovecot-ldap.conf.ext
# Définition de la base à partir de laquelle commencer les recherches
sed -i 's/^base =.*/base = '"${LDAP_BASE}"'/' /etc/dovecot/dovecot-ldap.conf.ext
# Autoriser la recherche dans les branches sous-jacentes de cette base
sed -i 's/^#scope = subtree.*/scope = subtree/' /etc/dovecot/dovecot-ldap.conf.ext
# Réaliser la correspondance entre l'UID (côté LDAP) et partie nom d'utilisateur de
# l'adresse mail (côté client).
sed -i 's/^#user_filter = .*/user_filter = '"${USER_FILTER}"'/' /etc/dovecot/dovecot-ldap.conf.ext
sed -i '130s/^.*/pass_attrs = uid=user,userPassword=password,uid=userdb_user/' /etc/dovecot/dovecot-ldap.conf.ext
sed -i 's/^#pass_filter = .*/pass_filter = '"${PASSWORD_FILTER}"'/' /etc/dovecot/dovecot-ldap.conf.ext
# On ne récupère aucun attribut car le nom du répertoire des mails correspond à la
# partie nom d'utilisateur de l'adresse mail. On n'a donc besoin d'aucun attribut.
echo "user_attrs =" >> /etc/dovecot/dovecot-ldap.conf.ext
#sed -i 's/^# = .*//' /etc/dovecot/dovecot-ldap.conf.ext


### Modification de /etc/dovecot/conf.d/auth-ldap.conf.ext
sed -i '15s/^#userdb {.*/userdb {/' /etc/dovecot/conf.d/auth-ldap.conf.ext
sed -i '16s/.*/  driver = prefetch/' /etc/dovecot/conf.d/auth-ldap.conf.ext
sed -i '17s/.*/}/' /etc/dovecot/conf.d/auth-ldap.conf.ext
#sed

### Modification de /etc/dovecot/conf.d/10-auth.conf
# Le mécanisme d'authentification ne se fait pas par le système mais par un LDAP (inclusion du fichier de config)
sed -i 's/^!include auth-system\.conf\.ext.*/#!include auth-system.conf.ext/' /etc/dovecot/conf.d/10-auth.conf
sed -i 's/^#!include auth-ldap\.conf\.ext.*/!include auth-ldap.conf.ext/' /etc/dovecot/conf.d/10-auth.conf
# Permet l'authentification en clair même si SSL/TLS n'est pas utilisé. À décommenter UNIQUEMENT pour des tests
# car les mots de passe sont transmis en clair sur le réseau !!
##sed -i 's/^#disable_plaintext_auth = yes.*/disable_plaintext_auth = no/' /etc/dovecot/conf.d/10-auth.conf
#sed -i 's/^# = .*//' /etc/dovecot/conf.d/10-auth.conf

### modification de /etc/dovecot/conf.d/10-logging.conf
# Redirection des logs vers stdout
sed -i 's/^#log_path = .*/log_path = \/dev\/stdout/' /etc/dovecot/conf.d/10-logging.conf
#sed -i 's/^# = .*//' /etc/dovecot/conf.d/10-logging.conf

### Modification de /etc/dovecot/conf.d/10-mail.conf
# Définition du format de stockage des mails (Maildir), du chemin pour l'emplacement
# de stockage des mails (/home/vmail/username/Maildir/) et du chemin pour l'emplacement
# de stockage des fichiers spécifiques à l'utilisateur. /!\ Il n'y aucun rapport avec
# le home directory d'UNIX! Voir https://wiki.dovecot.org/VirtualUsers/Home.
sed -i 's/^mail_location = .*/mail_location = maildir:\/home\/vmail\/%n\/Maildir\
mail_home = \/home\/vmail\/%n/' /etc/dovecot/conf.d/10-mail.conf
# Définition d'un UID et d'un GID générique pour tous les utilisateurs.
# Spécifique au fonctionnement d'utilisateur virtuel.
sed -i 's/^#mail_uid =.*/mail_uid = vmail/' /etc/dovecot/conf.d/10-mail.conf
sed -i 's/^#mail_gid =.*/mail_gid = vmail/' /etc/dovecot/conf.d/10-mail.conf
#sed -i 's/^# = .*//' /etc/dovecot/conf.d/10-mail.conf
mkdir /home/vmail
chmod +w /home/vmail
chown vmail /home/vmail

### Modification de /etc/dovecot/conf.d/10-master.conf
# Suppression de la connexion LMTP (communication avec Postfix) par socket UNIX
sed -i 's/^  unix_listener lmtp {.*/#  unix_listener lmtp {/' /etc/dovecot/conf.d/10-master.conf
sed -i 's/^    #mode = .*/#    #mode = 0666/' /etc/dovecot/conf.d/10-master.conf
sed -i '57s/^  }.*/  #}/' /etc/dovecot/conf.d/10-master.conf
# Définition de la connexion LMTP par paquets IP
sed -i 's/^  #inet_listener lmtp {.*/  inet_listener lmtp {/' /etc/dovecot/conf.d/10-master.conf
sed -i '62s/^    #address =.*/    address = 0.0.0.0/' /etc/dovecot/conf.d/10-master.conf
sed -i '63s/^    #port =.*/    port = 24/' /etc/dovecot/conf.d/10-master.conf
sed -i '64s/^  #}.*/  }/' /etc/dovecot/conf.d/10-master.conf
# Spécification des protocoles utilisés
echo "protocols = imap lmtp" >> /etc/dovecot/conf.d/10-master.conf
#sed -i 's/^# = .*//' /etc/dovecot/conf.d/10-master.conf

### Modification de /etc/dovecot/conf.d/10-ssl.conf
sed -i 's/^ssl = no.*/ssl = yes/' /etc/dovecot/conf.d/10-ssl.conf
sed -i 's#^ssl_cert = .*#ssl_cert = <'"${SSL_CERT}"'#' /etc/dovecot/conf.d/10-ssl.conf
sed -i 's#^ssl_key = .*#ssl_key = <'"${SSL_KEY}"'#' /etc/dovecot/conf.d/10-ssl.conf
#sed -i 's/^# = .*//' /etc/dovecot/conf.d/10-ssl.conf
