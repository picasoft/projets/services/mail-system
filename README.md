## Serveur mail de Picasoft

Ce dossier contient les ressources pour lancer le serveur mail de Picasoft.
Toutes les images sont personnalisées selon nos besoins et construites à la main.

Cette documentation se limite à Docker. Pour plus d'informations sur le serveur mail en général, voir [la documentation sur le Wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:mail:start).

### Construction des images

Deux images sont nécessaires pour le serveur mail : le MTA et le MDA.

On y adjoint un exporter compatible avec Prometheus pour récupérer des métriques sur le MTA, par exemple le nombre de mails envoyés, le nombre de mails rejetés, etc. Ces statistiques permettront de lever des alertes plus facilement qu'en vérifiant les logs à la main. Pour plus de détails sur la collecte de métriques chez Picasoft, voir [cette section du wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:metrologie:start).

Les trois Dockerfile sont présents dans les dossiers [pica-mail-mda](./pica-mail-mda), [pica-mail-mta](./pica-mail-mta) et [postfix-exporter](./postfix-exporter).
Pour plus de facilité, Docker Compose sait construire ces images simultanément : il suffit de lancer

```bash
docker-compose build
```

Puis de les pousser sur le registre de production :

```bash
docker-compose push
```

### Configuration

La configuration se fait essentiellement via les variables d'environnement de [Compose](./docker-compose.yml).
Deux dossiers supplémentaires doivent exister :

* `/DATA/docker/mail/opendkim`, dont le contenu sera peuplé [comme indiqué dans la documentation](https://wiki.picasoft.net/doku.php?id=technique:adminsys:mail:config:dkim).
* `/DATA/docker/certs/mail.picasoft.net`, qui doit contenir les certificats SSL du serveur mail. Ils sont gérés par [TLS Certs Monitor](../pica-tls-certs-monitor).

### Lancement

Copier `mail.secrets.example` dans `mail.secrets` et remplacer les valeurs. Le mot de passe LDAP du compte `mail` est sur le [pass](https://gitlab.utc.fr/picasoft/interne/pass).

S'assurer que [TLS Certs Monitor](../pica-tls-certs-monitor) est lancé.

Attetion, le serveur mail a besoin d'enregistrements DNS et de clés DKIM pour fonctionner. S'il est lancé pour la première fois, voir la page [déploiement du wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:mail:deploiement).

Enfin il faut créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir une variable `METRICS_AUTH`. Cette vairbale correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `METRICS_AUTH="mail:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."` Le mot de passe est à chiffrer avec bcrypt et à rajouter dans le [pass](https://wiki.picasoft.net/doku.php?id=technique:tips:password_store:start) (`Tech/Prometheus-Exporters-Auth/mail`).

Lancer :
```bash
docker-compose up -d
```

### Mise à jour

Il suffit de mettre à jour les Dockerfile et les tags dans le [fichier Compose](./docker-compose.yml). Il sera alors de bon ton de créer un fichier `CHANGELOG.md` pour expliquer les changements et d'ajouter un tag aux images pour faire la différence.

En cas de modification de la configuration, pas besoin de changer le tag.
