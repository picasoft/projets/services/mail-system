#!/bin/bash

set -e -o pipefail

# postconf permet de modifier /etc/postfix/main.cf

# 1. Identité
# Nom de domaine des courriers dont on est à l'origine (pour s'en réserver l'exclusivité)
postconf -e "mydomain = ${MY_DOMAIN}"
postconf -e "myorigin = ${MY_DOMAIN}"
# Nom d'hôte de la machine qui va faire tourner le MTA
postconf -e "myhostname=${MY_HOSTNAME}"

# 2. Boîtes mail
# Configuration du local delivery agent, ici c'est le MDA Dovecot, on communique avec le protocole LMTP sur TCP/IP (inet dans le jargon Postfix)
postconf -e "virtual_transport = lmtp:inet:${LMTP_LAN_HOSTNAME}:${LMTP_PORT}"
# Noms de domaines des boîtes gérées par ce serveur de fin de chaîne
postconf -e "virtual_mailbox_domains = ${MY_DOMAIN}, localhost.${MY_DOMAIN}, localhost"
# Pour chaque destinataire en picasoft.net, on consulte
# le LDAP pour savoir si l'adresse est valide
postconf -e "virtual_mailbox_maps = ldap:/etc/postfix/ldap-virtual-mailbox-maps"
# Pour chaque envoi de mail depuis une adresse picasoft.net (MAIL FROM),
# on consulte le LDAP pour savoir quel utilisateur SASL a le droit d'utiliser
# cette adresse (par défaut, lui seul)
postconf -e "smtpd_sender_login_maps = ldap:/etc/postfix/ldap-virtual-mailbox-maps"
# Utilisé pour construire la table utilisée ci-dessus (format adresse:UID).
# Si on trouve un compte LDAP qui correspond à l'adresse en question
# (avec la requête LDAP_VIRTUAL_MAILBOX_FILTER),
# on récupère son UID pour remplir la table (ce qui explique pourquoi
# un utilisateur a le droit de n'envoyer que pour lui même)
umask 037
cat <<EOF >> /etc/postfix/ldap-virtual-mailbox-maps
server_host = ${LDAP_PROTOCOL}://${LDAP_SERVER_HOSTNAME}:${LDAP_PORT}
search_base = ${LDAP_SEARCH_BASE}
query_filter = ${LDAP_VIRTUAL_MAILBOX_FILTER}
bind = yes
bind_dn = ${LDAP_BIND_DN}
bind_pw = ${LDAP_BIND_PW}
result_attribute = uid
# Mandatory for SSL : http://www.postfix.org/ldap_table.5.html
# Section LDAP SSL AND STARTTLS PARAMETERS
version = 3
EOF
chown root:postfix /etc/postfix/ldap-virtual-mailbox-maps

# On fait confiance à l'hôte local, utile pour faire du debug
postconf -e "mynetworks = 127.0.0.0/8"

# 3. Restrictions sur les destinataires
# Les personnes authentifiées peuvent envoyer des mails à tout le monde, les personnes non authentifiées peuvent envoyer des mails seulement aux destinations connues donc des destinataires en @picasoft.net
# On rejette si le destinataire n'a pas de MX ni de A et n'est pas nous-même
# L'ordre permit_sasl_authenticated, reject_unauth_destination est important: si on est authentifié on fait ce qu'on veut, si on n'est pas authentifié on ne peut qu'envoyer aux destinations connues
postconf -e 'smtpd_recipient_restrictions = reject_unknown_recipient_domain, permit_sasl_authenticated, reject_unauth_destination'

# Rejette les clients qui ne disent pas bonjour, ou qui disent
# bonjour (HELO ou EHLO) avec un nom de domaine non-valide, non pleinement
# qualifié (i.e. "absolu"), ou sans enregistrement DNS A ou MX.
postconf -e "smtpd_helo_restrictions = reject_invalid_helo_hostname"
postconf -e "smtpd_helo_required = yes"

# Blacklists basées sur l'adresse du client
postconf -e "smtpd_client_restrictions = ${SMTPD_CLIENT_RESTRICTIONS}"

# On interdit aux gens d'utiliser une adresse qui leur appartient pas
# (typiquement lorsque smtpd_sender_login_maps donne un propriétaire pour
# le MAIL FROM, mais que ce propriétaire ne correspond pas au compte SASL courant)
postconf -e "smtpd_sender_restrictions = reject_unauthenticated_sender_login_mismatch reject_authenticated_sender_login_mismatch"


# 4. Authentification SASL
# Utiliser le démon saslauthd. Il est contacté par des appels de fonction à une lib et retourne la validité des login.
# PLAIN est un des mécanismes définis pour l'auth SASL = authentication simple
# avec mot de passe en clair.
postconf -e "smtpd_sasl_path = smtpd"
cat <<EOF >> /etc/postfix/sasl/smtpd.conf
pwcheck_method: saslauthd
mech_list: PLAIN LOGIN
EOF
# Fichiers de config et socket utilisés par le démon de saslauthd créé pour postfix (voir fichier copié dans le Dockerfile)
adduser postfix sasl
# Activation du SASL
postconf -e 'smtpd_sasl_local_domain = '
postconf -e 'smtpd_sasl_auth_enable = yes'
# Autorise l'auth SASL seulement sur un canal TLS
postconf -e 'smtpd_tls_auth_only = yes'

# Autorise l'auth depuis des clients connus comme obsolètes/non standard (outlook) mais ne présentant pas de faille de sécurité
postconf -e 'broken_sasl_auth_clients = yes'
# Configuration de ce serveur LDAP
groupadd saslauthd
adduser --system --no-create-home --ingroup saslauthd saslauthd
cat <<EOF >> /etc/saslauthd.conf
ldap_servers: ${LDAP_PROTOCOL}://${LDAP_SERVER_HOSTNAME}:${LDAP_PORT}
ldap_bind_dn: ${LDAP_BIND_DN}
ldap_bind_pw: ${LDAP_BIND_PW}
ldap_search_base: ${LDAP_SEARCH_BASE}
ldap_filter: ${LDAP_SASL_FILTER}
EOF
chown root:saslauthd /etc/saslauthd.conf

# 5. Configuration DMARC
cat <<EOF >> /etc/opendmarc.conf
#ajout de mon nom d'hôte
TrustedAuthservIDs ${MY_HOSTNAME}
AuthservID ${MY_HOSTNAME}
#si le mail vient de quelqu'un (de chez picasoft) qui s'est connecté avec un client SMTP (un humain ou mattermost) alors son mail n'aura pas de headers spf/dkim, ce qui fait qu'il est invalide au vu de notre propre politique dmarc. On trust donc tous les gens qui se sont connectés en sasl et on les force à pass le dmarc.
IgnoreAuthenticatedClients true
EOF

# 6. Configuration DKIM
# Écriture des tables indiquant quoi signer avec quelle clef
/opendkim-tables.sh
postconf -e "milter_default_action = accept"
postconf -e "milter_protocol = 6"
postconf -e "smtpd_milters = unix:/var/run/opendkim/opendkim.sock, unix:/var/run/opendmarc/opendmarc.sock"
postconf -e "non_smtpd_milters = unix:/var/run/opendkim/opendkim.sock, unix:/var/run/opendmarc/opendmarc.sock"

# 7. Répertoires pour les sockets et permissions en général

# add users to groups
adduser postfix opendkim
adduser postfix opendmarc
adduser opendmarc postdrop

mkdir /var/run/postfix
postconf -e "queue_directory=/var/run/postfix"

chown postfix:postfix /var/run/postfix
chown opendkim:opendkim /etc/dkimkeys
chown opendkim:postfix /var/run/opendkim
chown opendmarc:postfix /var/run/opendmarc
chown saslauthd:postfix /var/run/saslauthd

# Workaround pour un problème de permission
touch /var/log/opendmarc.log
chown opendmarc:opendmarc /var/log/opendmarc.log
# So that opendmarc can write to maildrop folder

# configuration des logs vers stdout
postconf -e "maillog_file=/dev/stdout"

# 8. Chiffrement
# SSL : récupération des certificats (qui seront utilisés pour la connexion SMTP)
postconf -e "smtpd_tls_cert_file = ${SSL_CERT}"
postconf -e "smtpd_tls_key_file = ${SSL_KEY}"


# Autorise TLS pour des mails entrants et sortants mais n'interdit pas le clair lorsque ce n'est pas disponible
postconf -e "smtp_tls_security_level=may"
postconf -e "smtpd_tls_security_level=may"

# 10. Socket TCP/IP
# On est obligé d'utiliser ipv4 pour la local delivery car les docker network ne supportent pas iPv6 par défaut
postconf -e "inet_protocols = ipv4"

# 11. Rate limiting : vise à éviter le blacklist tout en gardant une fréquence d'envoi raisonnable
# Abaisse de 20 à 2 le nombre de connexion concurrentes à un même domaine
postconf -e "initial_destination_concurrency = 2"
postconf -e "smtp_destination_concurrency_limit = 2"
# Pas plus d'un mail toute les secondes à un même domaine
postconf -e "default_destination_rate_delay = 1s"
